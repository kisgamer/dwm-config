# dwm config
This repo is for my dwm dotfiles.
## Things needed
- JetBrains Mono font family: https://download.jetbrains.com/fonts/JetBrainsMono-2.304.zip
- autostart.sh file in ~/.local/dwm/
- libim2: arch: ```# pacman -S libim2```
- kitty
- There are more requirements, see: https://gitlab.com/kisgamer/xdg-autostart
